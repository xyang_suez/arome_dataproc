import numpy as np
from datetime import datetime
import pyproj
import netCDF4 as nc

class Point:
    def __init__(self, proj, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None):
            raise Exception('Error in Point class __init__(): initialize with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            self.lat, self.lon = latlon_tuple
            self.x = proj(self.lon, self.lat)[0]
            self.y = proj(self.lon, self.lat)[1]
        else:
            self.x, self.y = xy_tuple
            self.lon = proj(self.x, self.y, inverse=True)[0]
            self.lat = proj(self.x, self.y, inverse=True)[1]
        self.data_var3d = {}
        self.data_var4d = {}

    def distance(self, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None):
            raise Exception('Error in Point class distance(): calculate distance with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            lat, lon = latlon_tuple
            return np.sqrt((self.lat - lat)**2 + (self.lon - lon)**2)
        else:
            x, y = xy_tuple
            return np.sqrt((self.x - x)**2 + (self.y - y)**2)

    def latlon_to_xy(self, proj_str):
        local_proj = pyproj.Proj(proj_str)
        xy = local_proj(self.lon, self.lat)
        self.x = xy[0]
        self.y = xy[1]

    def xy_to_latlon(self, proj_str):
        local_proj = pyproj.Proj(proj_str)
        latlon = local_proj(self.x, self.y, inverse=True)
        self.lon = latlon[0]
        self.lat = latlon[1]

class UniformGrid:
    def __init__(self, ncvar, var3d_strings, var4d_strings, latlon=True, range_dir1=None, range_dir2=None, proj_str='epsg:32631'):
        self.is_latlon_based = latlon
        self.proj = pyproj.Proj(proj_str)  # default epsg code for Dunkirk, France
        self.latlon_dec = 5
        self.xy_dec = 0
        # Return numpy array instead of numpy masked_array:
        ncvar.set_auto_mask(False)
        # Extract time variable:
        time_var = ncvar.variables['time']
        self.datetimes = nc.num2date(time_var, units=time_var.units)
        print('NetCDF time starts on date: ' + self.datetimes[0].strftime('%Y-%m-%d') + '\n')
        # 1D arrays:
        if self.is_latlon_based:
            if 'lat' in ncvar.variables:
                lat = ncvar.variables['lat'][:]
            else:
                raise Exception('Error in UniformGrid class __init__(): no lat variable when self.is_latlon_based=True.')
            indices_dir2 = np.arange(len(lat))
            if range_dir2:
                lat_min, lat_max = range_dir2
                indices_dir2 = np.where((lat >= lat_min) & (lat <= lat_max))[0]
                lat = lat[indices_dir2]
            if 'lon' in ncvar.variables:
                lon = ncvar.variables['lon'][:]
            else:
                raise Exception('Error in UniformGrid class __init__(): no lon variable when self.is_latlon_based=True.')
            indices_dir1 = np.arange(len(lon))
            if range_dir1:
                lon_min, lon_max = range_dir1
                indices_dir1 = np.where((lon >= lon_min) & (lon <= lon_max))[0]
                lon = lon[indices_dir1]
            self.d_lat = lat[1] - lat[0]
            self.d_lon = lon[1] - lon[0]
            self.lat_array_uniform = lat
            self.lon_array_uniform = lon
        else:
            if 'x' in ncvar.variables:
                x = ncvar.variables['x'][:]
            elif 'X' in ncvar.variables:
                x = ncvar.variables['X'][:]
            else:
                raise Exception('Error in UniformGrid class __init__(): no x or X variable when self.is_latlon_based=False.')
            indices_dir1 = np.arange(len(x))
            if range_dir1:
                x_min, x_max = range_dir1
                indices_dir1 = np.where((x >= x_min) & (x <= x_max))[0]
                x = x[indices_dir1]
            if 'y' in ncvar.variables:
                y = ncvar.variables['y'][:]
            elif 'Y' in ncvar.variables:
                y = ncvar.variables['Y'][:]
            else:
                raise Exception('Error in UniformGrid class __init__(): no y or Y variable when self.is_latlon_based=False.')
            indices_dir2 = np.arange(len(y))
            if range_dir2:
                y_min, y_max = range_dir2
                indices_dir2 = np.where((y >= y_min) & (y <= y_max))[0]
                y = y[indices_dir2]
            self.d_x = x[1] - x[0]
            self.d_y = y[1] - y[0]
            self.x_array_uniform = x
            self.y_array_uniform = y
        if 'z' in ncvar.variables:
            self.altitudes = ncvar.variables['z'][:]
        elif 'Z' in ncvar.variables:
            self.altitudes = ncvar.variables['Z'][:]
        else:
            print('WARNING in UniformGrid class __init__(): z or Z variable missing. Default altitudes set to 10m and 20m.')
            self.altitudes = np.array([10., 20.])
        # 3D arrays:
        self.var3d_strings = var3d_strings
        dict_var3d = {}
        for var_name in self.var3d_strings:
            dict_var3d[var_name] = ncvar.variables[var_name][:, :, :]
        # 4D arrays:
        self.var4d_strings = var4d_strings
        dict_var4d = {}
        for var_name in self.var4d_strings:
            dict_var4d[var_name] = ncvar.variables[var_name][:, :, :, :]
        ncvar.close()

        num_dir2 = len(lat) if self.is_latlon_based else len(y)
        num_dir1 = len(lon) if self.is_latlon_based else len(x)
        self.nt = len(self.datetimes)
        self.nz = len(self.altitudes)

        # Initializing grid points with zeros:
        self.point_list = []
        for i in range(num_dir2):
            for j in range(num_dir1):
                if self.is_latlon_based:
                    self.point_list.append(Point(self.proj, latlon_tuple=(lat[i], lon[j])))
                else:
                    self.point_list.append(Point(self.proj, xy_tuple=(x[j], y[i])))

        # Find min and max for latlon and xy coordinates:
        self.lat_min = self.lon_min = self.x_min = self.y_min = float('inf')
        self.lat_max = self.lon_max = self.x_max = self.y_max = float('-inf')
        for gp in self.point_list:
            if gp.lat < self.lat_min:
                self.lat_min = gp.lat
            if gp.lat > self.lat_max:
                self.lat_max = gp.lat
            if gp.lon < self.lon_min:
                self.lon_min = gp.lon
            if gp.lon > self.lon_max:
                self.lon_max = gp.lon
            if gp.x < self.x_min:
                self.x_min = gp.x
            if gp.x > self.x_max:
                self.x_max = gp.x
            if gp.y < self.y_min:
                self.y_min = gp.y
            if gp.y > self.y_max:
                self.y_max = gp.y

        # Assigning values to all grid points:
        gp_id = -1
        for i in indices_dir2:
            for j in indices_dir1:
                gp_id += 1
                for var_name in self.var3d_strings:
                    self.point_list[gp_id].data_var3d[var_name] = dict_var3d[var_name][:, i, j].astype('float64')
                for var_name in self.var4d_strings:
                    self.point_list[gp_id].data_var4d[var_name] = dict_var4d[var_name][:, :, i, j].astype('float64')

        self.make_dicts()

    def make_dicts(self):
        self.point_latlon_dict = {}
        self.point_xy_dict = {}
        for gp in self.point_list:
            self.point_latlon_dict[self.__round_tuple((gp.lat, gp.lon), self.latlon_dec)] = gp
            self.point_xy_dict[self.__round_tuple((gp.x, gp.y), self.xy_dec)] = gp

    def at(self, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None):
            raise Exception('Error in UniformGrid.at(): search with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            if self.__round_tuple(latlon_tuple, self.latlon_dec) in self.point_latlon_dict:
                gp = self.point_latlon_dict[self.__round_tuple(latlon_tuple, self.latlon_dec)]
            else:
                gp = self.__interpolate(latlon_tuple=latlon_tuple)
        else:
            if self.__round_tuple(xy_tuple, self.xy_dec) in self.point_xy_dict:
                gp = self.point_xy_dict[self.__round_tuple(xy_tuple, self.xy_dec)]
            else:
                gp = self.__interpolate(xy_tuple=xy_tuple)
        return gp

    def point_to_string(self, gp, dt):
        fmt = '15.7e'
        if dt in self.datetimes:
            index_dt = self.datetimes.tolist().index(dt)
            o_string  = dt.strftime('%Y-%m-%d %H:%M:%S') + ','
            o_string += f"{gp.lat:{fmt}},"
            o_string += f"{gp.lon:{fmt}},"
            o_string += f"{gp.x:{fmt}},"
            o_string += f"{gp.y:{fmt}},"
            for var_name in self.var3d_strings:
                o_string += f"{gp.data_var3d[var_name][index_dt]:{fmt}},"
            for k in range(self.nz):
                for var_name in self.var4d_strings:
                    o_string += f"{gp.data_var4d[var_name][index_dt, k]:{fmt}},"
            o_string  = o_string[:-1] + '\n'
            return o_string
        else:
            raise Exception('Error in UniformGrid.point_to_string(): datetime value not found in NetCDF file.')

    def csv_header(self):
        o_header = 'Datetime,Latitude,Longitude,X,Y,'
        if self.var3d_strings:
            o_header += ','.join(self.var3d_strings) + ','
        if self.var4d_strings:
            for z in self.altitudes:
                var4d_strings_alt = [self.var4d_strings[i] + '_' + str(int(z)) + 'm' for i in range(len(self.var4d_strings))]
                o_header += ','.join(var4d_strings_alt) + ','
            o_header  = o_header[:-1] + '\n'
        return o_header

    def find_k_nearest_grid_points(self, k, latlon_tuple=None, xy_tuple=None):
        if k <= len(self.point_list):
            dist = []
            for gp in self.point_list:
                if (latlon_tuple is None) == (xy_tuple is None): 
                    raise Exception('Error in UniformGrid.find_k_nearest_grid_points(): find with either latlon_tuple or xy_tuple.')
                elif latlon_tuple:
                    dist.append(gp.distance(latlon_tuple=latlon_tuple))
                else:
                    dist.append(gp.distance(xy_tuple=xy_tuple))

            k_indices = np.argsort(dist)[:k] # get indices of the k nearest grid points
            return [self.point_list[i] for i in k_indices]
        else:
            raise Exception('Error in UniformGrid.find_k_nearest_grid_points(): k value exceeds grid point number.')

    def find_surrounding_grid_points(self, latlon_tuple=None, xy_tuple=None):
        surrounding_gps = []
        if (latlon_tuple is None) == (xy_tuple is None): 
            raise Exception('Error in UniformGrid.find_surrounding_grid_points(): find with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            lat, lon = latlon_tuple
            if lat <= self.lat_min or lat >= self.lat_max or lon <= self.lon_min or lon >= self.lon_max:
                raise Exception('Error in UniformGrid.find_surrounding_grid_points(): latlon_tuple out of bounds.')
            else:
                lat1 = self.lat_min + int((lat - self.lat_min)/self.d_lat)*self.d_lat
                lon1 = self.lon_min + int((lon - self.lon_min)/self.d_lon)*self.d_lon
                latlon_tuple1 = (lat1, lon1)
                latlon_tuple2 = (lat1 + self.d_lat, lon1)
                latlon_tuple3 = (lat1 + self.d_lat, lon1 + self.d_lon)
                latlon_tuple4 = (lat1, lon1 + self.d_lon)
                surrounding_gps = [self.at(latlon_tuple=latlon_tuple1),
                                   self.at(latlon_tuple=latlon_tuple2),
                                   self.at(latlon_tuple=latlon_tuple3),
                                   self.at(latlon_tuple=latlon_tuple4)]
        else:
            x, y = xy_tuple
            if x <= self.x_min or x >= self.x_max or y <= self.y_min or y >= self.y_max:
                raise Exception('Error in UniformGrid.find_surrounding_grid_points(): xy_tuple out of bounds.')
            else:
                x1 = self.x_min + int((x - self.x_min)/self.d_x)*self.d_x
                y1 = self.y_min + int((y - self.y_min)/self.d_y)*self.d_y
                xy_tuple1 = (x1, y1)
                xy_tuple2 = (x1 + self.d_x, y1)
                xy_tuple3 = (x1 + self.d_x, y1 + self.d_y)
                xy_tuple4 = (x1, y1 + self.d_y)
                surrounding_gps = [self.at(xy_tuple=xy_tuple1),
                                   self.at(xy_tuple=xy_tuple2),
                                   self.at(xy_tuple=xy_tuple3),
                                   self.at(xy_tuple=xy_tuple4)]
        return surrounding_gps

    def __linear_interpolation(self, gp1, gp2, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None): 
            raise Exception('Error in UniformGrid.__linear_interpolation(): interpolate with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            target_point = Point(self.proj, latlon_tuple=latlon_tuple)
            fac = target_point.distance(latlon_tuple=(gp1.lat, gp1.lon))/gp2.distance(latlon_tuple=(gp1.lat, gp1.lon))
        else:
            target_point = Point(self.proj, xy_tuple=xy_tuple)
            fac = target_point.distance(xy_tuple=(gp1.x, gp1.y))/gp2.distance(xy_tuple=(gp1.x, gp1.y))

        for var_name in self.var3d_strings:
            target_point.data_var3d[var_name] = gp1.data_var3d[var_name] + fac*(gp2.data_var3d[var_name] - gp1.data_var3d[var_name])
        for var_name in self.var4d_strings:
            target_point.data_var4d[var_name] = gp1.data_var4d[var_name] + fac*(gp2.data_var4d[var_name] - gp1.data_var4d[var_name])
        return target_point

    def __bilinear_interpolation(self, gp1, gp2, gp3, gp4, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None): 
            raise Exception('Error in UniformGrid.__bilinear_interpolation(): interpolate with either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            y0, x0 = latlon_tuple
            x1 = gp1.lon
            y1 = gp1.lat
            x2 = gp2.lon
            y2 = gp2.lat
            x3 = gp3.lon
            y3 = gp3.lat
            x4 = gp4.lon
            y4 = gp4.lat
            target_point = Point(self.proj, latlon_tuple=latlon_tuple)
        else:
            x0, y0 = xy_tuple
            x1 = gp1.x
            y1 = gp1.y
            x2 = gp2.x
            y2 = gp2.y
            x3 = gp3.x
            y3 = gp3.y
            x4 = gp4.x
            y4 = gp4.y
            target_point = Point(self.proj, xy_tuple=xy_tuple)

        xc = (x1 + x2 + x3 + x4)/4
        yc = (y1 + y2 + y3 + y4)/4
        mat = np.array([[1, x1-xc, y1-yc, (x1-xc)*(y1-yc)],
                        [1, x2-xc, y2-yc, (x2-xc)*(y2-yc)],
                        [1, x3-xc, y3-yc, (x3-xc)*(y3-yc)],
                        [1, x4-xc, y4-yc, (x4-xc)*(y4-yc)]])
        vec = np.array( [1, x0-xc, y0-yc, (x0-xc)*(y0-yc)] )
        cell_points = [gp1, gp2, gp3, gp4]
        for var_name in self.var3d_strings:
            target_point.data_var3d[var_name] = np.zeros(self.nt)
        for var_name in self.var4d_strings:
            target_point.data_var4d[var_name] = np.zeros((self.nt, self.nz))
        for t in range(self.nt):
            for var_name in self.var3d_strings:
                b = np.zeros(4)
                for i, p in enumerate(cell_points):
                    b[i] = p.data_var3d[var_name][t]
                a = np.linalg.solve(mat, b) # interpolation coefficients
                target_point.data_var3d[var_name][t] = np.dot(vec, a)
            for k in range(self.nz):
                for var_name in self.var4d_strings:
                    b = np.zeros(4)
                    for i, p in enumerate(cell_points):
                        b[i] = p.data_var4d[var_name][t, k]
                    a = np.linalg.solve(mat, b) # interpolation coefficients
                    target_point.data_var4d[var_name][t, k] = np.dot(vec, a)
        return target_point

    def __interpolate(self, latlon_tuple=None, xy_tuple=None):
        if (latlon_tuple is None) == (xy_tuple is None): 
            raise Exception('Error in UniformGrid.__interpolate(): interpolate to either latlon_tuple or xy_tuple.')
        elif latlon_tuple:
            if self.is_latlon_based:
                surrounding_points = self.find_surrounding_grid_points(latlon_tuple=latlon_tuple)
                target_point = self.__bilinear_interpolation(surrounding_points[0],
                                                             surrounding_points[1],
                                                             surrounding_points[2],
                                                             surrounding_points[3],
                                                             latlon_tuple=latlon_tuple)
            else:
                lat, lon = latlon_tuple
                xy_tuple = self.proj(lon, lat)
                surrounding_points = self.find_surrounding_grid_points(xy_tuple=xy_tuple)
                target_point = self.__bilinear_interpolation(surrounding_points[0],
                                                             surrounding_points[1],
                                                             surrounding_points[2],
                                                             surrounding_points[3],
                                                             xy_tuple=xy_tuple)
        else:
            if not self.is_latlon_based:
                surrounding_points = self.find_surrounding_grid_points(xy_tuple=xy_tuple)
                target_point = self.__bilinear_interpolation(surrounding_points[0],
                                                             surrounding_points[1],
                                                             surrounding_points[2],
                                                             surrounding_points[3],
                                                             xy_tuple=xy_tuple)
            else:
                x, y = xy_tuple
                lon, lat = self.proj(x, y, inverse=True)
                latlon_tuple = (lat, lon)
                surrounding_points = self.find_surrounding_grid_points(latlon_tuple=latlon_tuple)
                target_point = self.__bilinear_interpolation(surrounding_points[0],
                                                             surrounding_points[1],
                                                             surrounding_points[2],
                                                             surrounding_points[3],
                                                             latlon_tuple=latlon_tuple)
        return target_point

    def __round_tuple(self, tup, dec=5):
        return (round(tup[0], dec), round(tup[1], dec))